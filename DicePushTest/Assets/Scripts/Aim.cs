using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Aim : MonoBehaviour
{
    [SerializeField] private Camera currentCamera;
    [SerializeField] private MeshRenderer renderer;
    [SerializeField] private SpawnDice spawnDice;
    [SerializeField] private AimTaril aimTrail;
    [SerializeField] private Image timeImage;
    private Dice currentDice;  
    private Vector3 startPosition;
    private float trailLenght;

    private bool ready;
    private float timer = 3f;
    private void Start()
    {
        renderer.enabled = false;
        startPosition = new Vector3 (0,0,-6.5f);
        ready = true;
    }

    private void Update()
    {
        currentDice = spawnDice.CurrentDice;
        if (currentDice && ready)
        {
            if (Input.GetMouseButton(0))
            {
                renderer.enabled = true;
                Ray ray = currentCamera.ScreenPointToRay(Input.mousePosition);
                Debug.DrawRay(ray.origin, ray.direction * 10f);
                Plane plane = new Plane(Vector3.up, Vector3.up);
                float distance;
                plane.Raycast(ray, out distance);
                Vector3 point = ray.GetPoint(distance);

                float distanceAim = Vector3.Distance(startPosition, transform.position);
                float interpolat = Mathf.InverseLerp(0f, 10f, distanceAim);
                float scale = Mathf.Lerp(0f, 8f, interpolat);
                transform.position = point + Vector3.forward * scale;
                transform.rotation = Quaternion.Euler(90, 0, 0);

                trailLenght = Mathf.Lerp(5, 20, interpolat);
                aimTrail.DrawLine(currentDice.transform.position, transform.position, trailLenght);
            }
            if (Input.GetMouseButtonUp(0))
            {
                currentDice.SetTargetPoint(transform.position, trailLenght);
                StartCoroutine(HideAim());
                ready = false;
            }
        }  
        if(!ready)
        {
            timer -= Time.deltaTime;
            if (timer <= 0)
            {
                timer = 3f;
                
                ready = true;
            }
        }
        timeImage.fillAmount = timer / 3;
    }

    private IEnumerator HideAim()
    {
        yield return new WaitForSeconds(0.5f);
        renderer.enabled = false;
        aimTrail.DeleteLine();
    }
}
