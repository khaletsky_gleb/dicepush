using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitSpawn : MonoBehaviour
{
    [SerializeField] private Unit unitPlayer;
    [SerializeField] private Unit unitEnemy;
    [SerializeField] private GameObject playerSide;
    [SerializeField] private GameObject enemySide;

    private static UnitSpawn _current;
    public static UnitSpawn Currnet => _current;

    private void Awake()
    {
        _current = this;
    }

    public void CreateUnit(int unitNumber, Vector3 point, State state)
    {
        if(state == State.Player)
        {
            SpawnPlayer(unitNumber, point);
        }
        else
        {
            SpawnEnemy(unitNumber, point);
        }
    }
    private void SpawnPlayer(int unitNumber, Vector3 point)
    {
        for (int i = 0; i < unitNumber; i++)
        {
            Unit newUnit = Instantiate(unitPlayer, point, Quaternion.identity);
            newUnit.SetPointInstantiate(playerSide.transform.position);
            newUnit.SetTarget(playerSide.transform);
        }
    }

    private void SpawnEnemy(int unitNumber, Vector3 point)
    {
        for (int i = 0; i < unitNumber; i++)
        {
            Unit newUnit = Instantiate(unitEnemy, point, Quaternion.identity);
            newUnit.SetPointInstantiate(enemySide.transform.position);
            newUnit.SetTarget(enemySide.transform);
        }
    }
}
