using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Win : MonoBehaviour
{
    [SerializeField] private GameObject confeti;
    [SerializeField] private Text textUi;
    [SerializeField] private GameObject ui;
    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<MoveBar>())
        {
            ui.SetActive(true);
            Debug.Log("Win");
            Instantiate(confeti);
            textUi.text = "You Win!!!";
            Time.timeScale = 0f;

        }
    }
}
