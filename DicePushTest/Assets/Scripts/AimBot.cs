using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class AimBot : MonoBehaviour
{
    [SerializeField] private MeshRenderer renderer;
    [SerializeField] private SpawnDice spawnDice;
    [SerializeField] private AimTaril aimTrail;
    private Dice currentDice;
    private Vector3 target;
    private float trailLenght;
    private bool throwed;

    private void Start()
    {
        renderer.enabled = false;
        StartCoroutine(CalculateTarget());
    }
    private void ShowAim()
    {
        throwed = false;
        currentDice = spawnDice.CurrentDice;
        renderer.enabled = true;
        float distanceAim = Vector3.Distance(spawnDice.transform.position, transform.position);
        float interpolat = Mathf.InverseLerp(0f, 10f, distanceAim);
        trailLenght = Mathf.Lerp(5, 20, interpolat);
        transform.DOMove(target, 1).OnComplete(() => ThrowDice());
    }
    private void Update()
    {
        Debug.Log(trailLenght);
        if (renderer.enabled == true && !throwed)
        {
            aimTrail.DrawLine(currentDice.transform.position, transform.position, trailLenght);
        }        
    }

    private IEnumerator CalculateTarget()
    {
        yield return new WaitForSeconds(4);
        target = new Vector3(Random.Range(-2, 3), 1f, (Random.Range(1, 6)));
        ShowAim();
    }

    private void ThrowDice()
    {
        throwed = true;
        currentDice.SetTargetPoint(target, trailLenght);
        StartCoroutine(CalculateTarget());
        StartCoroutine(HideAim());
    }
    private IEnumerator HideAim()
    {
        yield return new WaitForSeconds(0.5f);
        renderer.enabled = false;
        aimTrail.DeleteLine();
    }
}
