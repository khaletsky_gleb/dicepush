using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Lose : MonoBehaviour
{
    [SerializeField] private Text textUi;
    [SerializeField] private GameObject ui;
    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<MoveBar>())
        {
            ui.SetActive(true);
            textUi.text = "You Lose:(";
            Time.timeScale = 0f;

        }
    }
}
