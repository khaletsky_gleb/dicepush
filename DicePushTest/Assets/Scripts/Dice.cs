using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public enum State
{
    Player,
    Enemy
}
public class Dice : MonoBehaviour
{
    [SerializeField] private Rigidbody rigidbody;
    [SerializeField] private float torquePower = 2.5f;
    [SerializeField] private State currentState;
    [SerializeField] private GameObject popPrefab;
    private Vector3 targetPosition;
    private bool thrown;
    public int value;
    private void Start()
    {
        thrown = false;
    }
    public void SetTargetPoint(Vector3 point, float lenght)
    {
        targetPosition = point;
        ThrowCube(lenght);
    }

    public void SetState(State state)
    {
        currentState = state;
    }
    private void ThrowCube(float lenght)
    {
        rigidbody.AddRelativeTorque(new Vector3(Random.Range(-1, 2), Random.Range(-1, 2), Random.Range(-1, 2)) * torquePower, ForceMode.VelocityChange);
        rigidbody.velocity = targetPosition;
        rigidbody.AddRelativeForce(new Vector3(targetPosition.x, 10, lenght + 2), ForceMode.VelocityChange);
        thrown = true;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<DeadZone>())
        {
            Destroy(gameObject);
        }
    }


    private void Update()
    {
        if (thrown)
        {
            if (rigidbody.velocity.y == 0)
            {
                value = getValue();
                CreateUnit();
            }
        }

    }

    public int getValue()
    {
        int iValue = 0;
        Vector3 dieRotation = gameObject.transform.eulerAngles;

        dieRotation = new Vector3(Mathf.RoundToInt(dieRotation.x), Mathf.RoundToInt(dieRotation.y), Mathf.RoundToInt(dieRotation.z));

        if (dieRotation.x == 0)
        {
            iValue = 6;
        }
        if (dieRotation.x == 90)
        {
            iValue = 2;
        }
        if (dieRotation.z == 180)
        {
            iValue = 1;
        }
        if (dieRotation.x == 270)
        {
            iValue = 5;
        }
        if (dieRotation.z == 90)
        {
            iValue = 4;
        }
        if (dieRotation.z == 270)
        {
            iValue = 3;
        }
        return iValue;
    }

    private void CreateUnit()
    {
        Instantiate(popPrefab, transform.position, Quaternion.identity);
        Destroy(gameObject);
        UnitSpawn.Currnet.CreateUnit(value, transform.position, currentState);
        
    }

}
