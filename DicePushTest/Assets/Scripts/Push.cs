using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Push : MonoBehaviour
{
    [SerializeField] private MoveBar moveBar;
    [SerializeField] private float speed;
    [SerializeField] private List<Unit> units = new List<Unit>();
    
    private void OnTriggerEnter(Collider other)
    {
        Unit unit = other.GetComponent<Unit>();
        if (unit)
        {
            if (!units.Contains(unit))
            {
                units.Add(unit);         
                AddSpeed();
            }
        }
    }

    private void AddSpeed()
    {
        float newSpeed = (speed / units.Count) + speed;
        moveBar.SetSpeed(newSpeed);
    }


}
