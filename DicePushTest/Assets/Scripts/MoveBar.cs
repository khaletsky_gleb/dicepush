using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveBar : MonoBehaviour
{
    [SerializeField] private Rigidbody rigidbody;
    [SerializeField] private float speed;
    [SerializeField] private float maxSpeed;
    [SerializeField] private float minSpeed;

    public void SetSpeed(float value)
    {
        speed += value;
        
    }
    private void FixedUpdate()
    {
        rigidbody.velocity = Vector3.forward * speed;
    }
}
