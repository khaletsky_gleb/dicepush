using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.AI;
public class Unit : MonoBehaviour
{
    [SerializeField] private Transform target;
    [SerializeField] private NavMeshAgent navMeshAgent;
    [SerializeField] private State _currentState;
    public State CurrentState => _currentState;

    public void SetPointInstantiate(Vector3 point)
    {
        navMeshAgent.SetDestination(point);
    }
    public void SetTarget(Transform point)
    {
        target = point;
    }
    private void FixedUpdate()
    {
        navMeshAgent.SetDestination(new Vector3(transform.position.x,transform.position.y, target.position.z));
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.GetComponent<Unit>())
        {
            Unit unit = other.GetComponent<Unit>();
            if (unit.CurrentState != _currentState)
            {
                Debug.Log("++++");
                Destroy(gameObject);
                Destroy(unit);
            }
        }
        
    }
}
