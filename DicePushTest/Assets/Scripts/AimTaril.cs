using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AimTaril : MonoBehaviour
{
    [SerializeField] private LineRenderer line;
    [SerializeField] private int segments = 10;

    public void DrawLine(Vector3 startPoint, Vector3 endPoint, float lenght)
    {
        float intepolant = Vector3.Distance(startPoint, endPoint) / lenght;
        float offset = Mathf.Lerp(lenght / 2, 0, intepolant);

        Vector3 startUp = startPoint + Vector3.up * offset;
        Vector3 endUp = endPoint + Vector3.up * offset;

        line.positionCount = segments + 1;
        for (int i = 0; i < segments+1; i++)
        {
            line.SetPosition(i, Bezier.GetPoint(startPoint,startUp,endPoint,(float)i/segments));
        }
    }
    public void DeleteLine()
    {
        line.positionCount = 0;
    }
}
