using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class SpawnDice : MonoBehaviour
{
    [SerializeField] private Dice dicePrefab;
    [SerializeField] private Vector3 startRotatation;
    [SerializeField] private State state;
    private Dice _currntDice;
    public Dice CurrentDice => _currntDice;

    private void Update()
    {
        if(_currntDice == null)
        {
            Spawn();
        }
    }
    public void Spawn()
    {
        _currntDice = Instantiate(dicePrefab, transform.position, Quaternion.Euler(startRotatation));
        _currntDice.SetState(state);
    }
}
